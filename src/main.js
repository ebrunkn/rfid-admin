// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import App from './App'
import router from './router'
import VueResource from 'vue-resource'
import Auth from './packages/auth/Auth.js'

Vue.use(BootstrapVue)
Vue.use(VueResource)
Vue.use(Auth)

Vue.http.options.root = 'http://localhost:8000'
Vue.http.headers.common['Authorization'] = 'Bearer '+ Vue.auth.getToken();

router.beforeEach(
    (to, from, next) => {
        if(to.matched.some(records => records.meta.forVisitor)) {
            if(Vue.auth.isAuth()) {
                next({
                    path:'/dashboard'
                })
            }else{
                next()
            }
        }
        else if(to.matched.some(records=>records.meta.forAuth)) {
            if(!Vue.auth.isAuth()){
                next({
                    path:'/login'
                })
            }else{
                next()
            }
        }
        else{
            next()
        }
    }
)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    components: {App},
    template: '<App/>'
})
