import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/auth/Login.vue';
import Dashboard from '@/components/dashboard/dashboard.vue';
import Products from '@/components/product/ProductList.vue'
import AddProduct from '@/components/product/AddProduct.vue'
import RfidConnect from '@/components/product/RfidConnect.vue'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'Landing',
            component: Login,
            meta : {
                forVisitor:true
            }
        },
        {
            path: '/login',
            name: 'Login',
            component: Login,
            meta : {
                forVisitor:true
            }
        },
        {
            path: '/dashboard',
            name: 'Dashboard',
            component: Dashboard,
            meta : {
                forAuth:true
            }
        },
        {
            path: '/products',
            name: 'Products',
            component: Products,
            meta : {
                forAuth:true
            }
        },
        {
            path: '/products/add',
            name: 'Add Product',
            component: AddProduct,
            meta : {
                forAuth:true
            }
        },
        {
            path: '/products/link/rfid',
            name: 'Connect Product',
            component: RfidConnect,
            meta : {
                forAuth:true
            }
        },

    ],

    mode:'history'
})
