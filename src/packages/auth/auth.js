export default function (Vue) {
    Vue.auth = {
        //set token
        setToken(token, expiration) {
            localStorage.setItem('token', token);
            localStorage.setItem('expiry', expiration);
        },
        //get token
        getToken() {
            var token = localStorage.getItem('token');
            var expiry = localStorage.getItem('expiry');
            if(!token || !expiry) {
                return null
            }
            if(Date.now() > parseInt(expiry)){
                this.deleteToken()
                return null
            }else {
                return token
            }
        },
        //delete token
        deleteToken() {
            localStorage.removeItem('token');
            localStorage.removeItem('expiry');
        },
        //is authenticated
        isAuth() {
            if(this.getToken()) {
                return true;
            }else {
                return false;
            }
        }
    }
    Object.defineProperties(Vue.prototype, {
        $auth: {
            get() {
                return Vue.auth;
            }
        }
    });
}